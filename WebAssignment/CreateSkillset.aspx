﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdmin.Master" AutoEventWireup="true" CodeBehind="CreateSkillset.aspx.cs" Inherits="WebAssignment.Createskillset" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p>
        Add your skillset in the box provided, separate commas for each skill:
    </p>
    <p>
        &nbsp;<asp:TextBox ID="txtSkillset" runat="server" Width="877px" Height="169px"></asp:TextBox> &nbsp;&nbsp;
        </p>
    <p>
        &nbsp;</p>
    <p>
        <asp:Button ID="btnConfirm" runat="server" Text="Confirm" OnClick="btnConfirm_Click" />
    </p>
</asp:Content>
