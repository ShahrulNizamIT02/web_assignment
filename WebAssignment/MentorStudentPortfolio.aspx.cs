﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using WebAssignment.App_Code;

namespace WebAssignment
{
    public partial class MentorStudentPortfolio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["LoginID"] == null)
            {
                Response.Redirect("MentorLogin.aspx");

            }
            if (!IsPostBack)
            {
                DisplayStudentList();
                DisplayStudentProject();
                DisplayProject();
            }
            

        }


      
        protected void gvStudent_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvStudent.PageIndex = e.NewPageIndex;
            DisplayStudentList();


        }
        public void DisplayStudentList()
        {
            //Read connection string "NPBookConnectionString" from web.config file
            string strConn = ConfigurationManager.ConnectionStrings["ABCConnectionString"].ToString();

            //Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object, supply it with the SQL statement
            //SELECT that operates against the database, and the connection object
            //used for connecting to the database
            SqlCommand cmd = new SqlCommand("SELECT * FROM Student  INNER JOIN Mentor ON Student.MentorID = Mentor.MentorID WHERE Mentor.EmailAddr = @email", conn);
            cmd.Parameters.AddWithValue("@email", Session["LoginID"]);

            //Instantiate a DataAdapter object and pass the SqlCommand object
            //created as parameter
            SqlDataAdapter daStudent = new SqlDataAdapter(cmd);

            //Create a Dataset object to contain the records retrieved from database
            DataSet result = new DataSet();

            //A connection must be opened before any operation made.
            conn.Open();
            //Use DataAdapter to fetch data to a table "StaffDetails" in DataSet.
            //DataSet "result" will store the result of the SELECT operation
            daStudent.Fill(result, "StudentDetails");
            //A connection should always be closed, whether error occurs or not.
            conn.Close();
            //Specify GridView to get data from table "StaffDetails"
            //in DataSet "result"
            gvStudent.DataSource = result.Tables["StudentDetails"];
            //Display the list of data in GridView
            gvStudent.DataBind();
        }
        public void DisplayStudentProject()
        {
            //Read connection string "NPBookConnectionString" from web.config file
            string strConn = ConfigurationManager.ConnectionStrings["ABCConnectionString"].ToString();

            //Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object, supply it with the SQL statement
            //SELECT that operates against the database, and the connection object
            //used for connecting to the database
            SqlCommand cmd = new SqlCommand("SELECT DISTINCT ProjectID,Student.Name,Role,Reflection FROM Student INNER JOIN Mentor ON Student.MentorID = Mentor.MentorID INNER JOIN ProjectMember ON ProjectMember.StudentID = Student.StudentID WHERE Mentor.EmailAddr = @email", conn);
            cmd.Parameters.AddWithValue("@email", Session["LoginID"]);

            //Instantiate a DataAdapter object and pass the SqlCommand object
            //created as parameter
            SqlDataAdapter daStudent = new SqlDataAdapter(cmd);

            //Create a Dataset object to contain the records retrieved from database
            DataSet result = new DataSet();

            //A connection must be opened before any operation made.
            conn.Open();
            //Use DataAdapter to fetch data to a table "StaffDetails" in DataSet.
            //DataSet "result" will store the result of the SELECT operation
            daStudent.Fill(result, "StudentProject");
            //A connection should always be closed, whether error occurs or not.
            conn.Close();
            //Specify GridView to get data from table "StaffDetails"
            //in DataSet "result"
            gvStudentProject.DataSource = result.Tables["StudentProject"];
            //Display the list of data in GridView
           gvStudentProject.DataBind();
        }

        public void DisplayProject()
        {
            //Read connection string "NPBookConnectionString" from web.config file
            string strConn = ConfigurationManager.ConnectionStrings["ABCConnectionString"].ToString();

            //Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object, supply it with the SQL statement
            //SELECT that operates against the database, and the connection object
            //used for connecting to the database
            SqlCommand cmd = new SqlCommand("SELECT DISTINCT Title,Description, ProjectPoster , ProjectURL FROM Project INNER JOIN ProjectMember ON ProjectMember.ProjectID = Project.ProjectID", conn);
            cmd.Parameters.AddWithValue("@email", Session["LoginID"]);

            //Instantiate a DataAdapter object and pass the SqlCommand object
            //created as parameter
            SqlDataAdapter daStudent = new SqlDataAdapter(cmd);

            //Create a Dataset object to contain the records retrieved from database
            DataSet result = new DataSet();

            //A connection must be opened before any operation made.
            conn.Open();
            //Use DataAdapter to fetch data to a table "StaffDetails" in DataSet.
            //DataSet "result" will store the result of the SELECT operation
            daStudent.Fill(result, "Project");
            //A connection should always be closed, whether error occurs or not.
            conn.Close();
            //Specify GridView to get data from table "StaffDetails"
            //in DataSet "result"
            gvProject.DataSource = result.Tables["Project"];
            //Display the list of data in GridView
            gvProject.DataBind();
        }

        protected void btnUpdateStatus_Click(object sender, EventArgs e)
        {
            foreach(GridViewRow row in gvStudent.Rows)
            {
                DropDownList dlist = (row.Cells[5].FindControl("ddlStatus") as DropDownList);
                int studentid = Convert.ToInt32(row.Cells[0].Text);
                updateStatus(studentid, dlist.Text);

            }
            gvStudent.DataBind();
            DisplayStudentList();
        }
        private void updateStatus(int studentid, string status)
        {
            string strConn = ConfigurationManager.ConnectionStrings["ABCConnectionString"].ToString();
            String updatedata = "Update Student set Status='" + status + "' where StudentID=" + studentid;
            SqlConnection conn = new SqlConnection(strConn);
            conn.Open();
            SqlCommand cmd = new SqlCommand(updatedata);
            cmd.CommandText = updatedata;
            cmd.Connection = conn;
            cmd.ExecuteNonQuery();
            conn.Close();
            lblStatus.Text = "Data Has Been Updated";
        }


        //protected void gvStudent_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    if(e.CommandName == "ApproveStatus")
        //    {
        //        int i = Convert.ToInt32(e.CommandArgument);
        //        string status = gvStudent.Rows[i].Cells[5].Text;

        //        //Retrieve the row index stored in the CommandArgument property

        //        //Get the Branch No from the second column
        //        int studentid = Convert.ToInt32(gvStudent.Rows[i].Cells[0].Text);
        //        //Call "updateStatus" method in "Mentor"class
        //        Mentor mentor = new Mentor();

        //    }
        //}
    }
}