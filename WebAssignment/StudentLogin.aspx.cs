﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace WebAssignment
{
    public partial class StudentLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string loginID = txtLoginID.Text;//Textbox: txtLoginID
            string password = txtPassword.Text;//Textbox:txtPassword
            try
            {

                if (password == "p@55Student")
                {

                    Session["loginEmail"] = loginID;
                    Session["LoggedInTime"] = DateTime.Now;
                    Session["type"] = "Student";
                    string strConn = ConfigurationManager.ConnectionStrings["ABCConnectionString"].ToString();
                    SqlConnection conn = new SqlConnection(strConn);
                    SqlCommand cmd = new SqlCommand("SELECT StudentID FROM Student WHERE EmailAddr = @emailaddr AND Password = @password", conn);
                    //SqlCommand cmd1 = new SqlCommand("select ProjectMember.Role from ProjectMember" +
                    //    "INNER JOIN Student ON ProjectMember.StudentID = Student.StudentID" +
                    //    "Where EmailAddr = @emailaddr and ProjectMember.Role = @role", conn);
                    //cmd1.Parameters.AddWithValue("@role",RoleID);
                    //cmd1.Parameters.AddWithValue("@emailaddr", loginID);

                    cmd.Parameters.AddWithValue("@emailaddr", loginID);
                    cmd.Parameters.AddWithValue("@password", password);
                    conn.Open();
                    int id = (int)cmd.ExecuteScalar();
                    conn.Close();
                    Session["loginID"] = id;
                    Response.Redirect("Profile.aspx");
                }
                else
                {
                    lblMessage.Text = "Incorrect User Credential";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                }
            }
            catch
            {
                return;
            }
            
         
           
        }
    }
}