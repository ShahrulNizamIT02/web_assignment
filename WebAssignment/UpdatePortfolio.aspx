﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student.Master" AutoEventWireup="true" CodeBehind="UpdatePortfolio.aspx.cs" Inherits="WebAssignment.UpdatePortfolio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="w-100">
    <tr>
        <td>&nbsp;</td>
        <td class="auto-style1"><strong>Update Project Portfolio</strong></td>
    </tr>
    <tr>
        <td>Title:</td>
        <td>
            <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>Description:</td>
        <td>
            <asp:TextBox ID="txtDescription" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>Poster Image:</td>
        <td>
            <asp:FileUpload ID="ImageUpload" runat="server" />
        </td>
    </tr>
    <tr>
        <td>URL:</td>
        <td>
            <asp:TextBox ID="txtUrl" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>Reflection:</td>
        <td>
            <asp:TextBox ID="txtReflection" runat="server" TextMode="MultiLine"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>
            <asp:Button ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" Text="Update" />
        </td>
    </tr>
</table>
</asp:Content>
