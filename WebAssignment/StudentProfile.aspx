﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student.Master" AutoEventWireup="true" CodeBehind="StudentProfile.aspx.cs" Inherits="WebAssignment.StudentProfile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 172px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblView" runat="server" ForeColor="Red" style="font-weight: 700" Text="View Student"></asp:Label>
<table class="w-100">
    <tr>
        <td>StudentID:</td>
        <td>
            <asp:Label ID="lblStudentID" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>Name:</td>
        <td>
            <asp:Label ID="lblName" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>Course:</td>
        <td>
            <asp:Label ID="lblCourse" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>Email:</td>
        <td>
            <asp:Label ID="lblEmail" runat="server"></asp:Label>
            <br />
        </td>
    </tr>
    <tr>
        <td>Description:</td>
        <td>
            <asp:TextBox ID="tbDescription" runat="server" Height="115px" TextMode="MultiLine" Width="450px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ControlToValidate="tbDescription" ErrorMessage="Description cannot be blank" ForeColor="Red"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>Achievement:</td>
        <td>
            <asp:TextBox ID="tbAchievement" runat="server" Height="115px" TextMode="MultiLine" Width="450px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvAchievement" runat="server" ControlToValidate="tbAchievement" ErrorMessage="Achievement cannot be blank" ForeColor="Red"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style1">ExternalLink:</td>
        <td class="auto-style1">
            <asp:TextBox ID="tbExternalLink" runat="server" Height="115px" Width="450px"></asp:TextBox>
            <br />
            <asp:RequiredFieldValidator ID="rfvExternalLink" runat="server" ControlToValidate="tbExternalLink" ErrorMessage="External Link cannot be blank" ForeColor="Red"></asp:RequiredFieldValidator>
            &nbsp;<br />
            <asp:RegularExpressionValidator ID="revLink" runat="server" ControlToValidate="tbExternalLink" ErrorMessage="Please enter a valid link" ForeColor="Red" ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?"></asp:RegularExpressionValidator>
            <br />
        </td>
    </tr>
    <tr>
        <td>Skill Set:</td>
        <td>
            <asp:CheckBoxList ID="cbSkill" runat="server">
            </asp:CheckBoxList>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>
            <asp:Button ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" Text="Update" />
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </td>
    </tr>
</table>
    </asp:Content>

