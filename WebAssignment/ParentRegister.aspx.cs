﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAssignment
{
    public partial class ParentRegister : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Parent objParent = new Parent();

                objParent.Name = txtPName.Text;
                objParent.EmailAddr = txtPEmail.Text;
                objParent.Password = txtPPassword.Text;
                objParent.add();

                Response.Redirect("RegisterConfirm.aspx?" + "&name=" + objParent.Name + "&emailAddr=" + objParent.EmailAddr);
            }
        }
    }
}