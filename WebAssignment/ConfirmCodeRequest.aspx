﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdmin.Master" AutoEventWireup="true" CodeBehind="ConfirmCodeRequest.aspx.cs" Inherits="WebAssignment.ConfirmCodeRequest" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p>
        You have 15 minutes to enter the code. Otherwise, you will be redirected to the previous page.</p>
    <p>
        &nbsp;</p>
    <p>
        Code:
        <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvCode" runat="server" ControlToValidate="txtCode" Display="Dynamic" ErrorMessage="*This field is required to be filled in"></asp:RequiredFieldValidator>
    </p>
    <p>
        <asp:Button ID="btnSubmit" runat="server" Text="Submit Code" />
    </p>
    <p>
        &nbsp;</p>
</asp:Content>
