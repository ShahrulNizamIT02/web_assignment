﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace WebAssignment
{
    public partial class ParentLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string conn = WebConfigurationManager.ConnectionStrings["ABCConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(conn);
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM Parent WHERE EmailAddr='" + txtLoginID.Text + "' AND Password ='" + txtPassword.Text + "' ", con);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                cmd.ExecuteNonQuery();

                if (dt.Rows[0][0].ToString() == "1")
                {
                    Session["LoginID"] = txtLoginID.Text;
                    Session["LoggedInTime"] = DateTime.Now;
                    //Redirect user to Main.aspx page
                    Response.Redirect("ViewChild.aspx");
                }
                else
                {
                    Response.Write("<script>alert('error in login')</script>");
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }
    }
}