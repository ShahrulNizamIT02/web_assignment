﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;


namespace WebAssignment
{
    public partial class AddStudent : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if  (!Page.IsPostBack)
            {
                ddlCourse.Items.Add("Business");
                ddlCourse.Items.Add("Humanities");
                ddlCourse.Items.Add("Film Studies");
                ddlCourse.Items.Add("Engineering");
                ddlCourse.Items.Add("Information & Communications Technology");
                ddlCourse.Items.Add("Health Sciences");

                displayMentor();
            }
        }
        
        protected void btn_Confirm_Click(object sender, EventArgs e)

        { 

            Student objStudent = new Student();

            objStudent.name = txtName.Text;
            objStudent.email = txtEmail.Text;
            objStudent.course = ddlCourse.SelectedValue;
            objStudent.mentorID = Convert.ToInt32(ddlMentor.SelectedValue);

            int id = objStudent.add();



            Response.Redirect("ConfirmAddStudent.aspx?ID=" + id + 
                               "&name=" + txtName.Text + "&email=" + txtEmail.Text + 
                                "&course=" + ddlCourse.SelectedValue);
        }

        public void displayMentor()
        {

            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();

            SqlConnection conn = new SqlConnection(strConn);

            SqlCommand cmd = new SqlCommand
                ("SELECT * FROM Mentor", conn);

            SqlDataAdapter daMentor = new SqlDataAdapter(cmd);

            DataSet result = new DataSet();

            conn.Open();

            daMentor.Fill(result, "MentorDetails");

            conn.Close();

            ddlMentor.DataSource = result.Tables["MentorDetails"];

            ddlMentor.DataTextField = "Name";

            ddlMentor.DataValueField = "MentorID";

            ddlMentor.DataBind();

            ddlMentor.Items.Insert(0, "--Select--");
        }
    }
}