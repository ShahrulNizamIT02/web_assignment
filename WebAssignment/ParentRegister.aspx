﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ParentRegister.aspx.cs" Inherits="WebAssignment.ParentRegister" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 202px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            CREATE PARENT ACCOUNT<table class="auto-style1">
                <tr>
                    <td class="auto-style2">Name :</td>
                    <td>
                        <asp:TextBox ID="txtPName" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">Email Address : </td>
                    <td>
                        <asp:TextBox ID="txtPEmail" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">Password : </td>
                    <td>
                        <asp:TextBox ID="txtPPassword" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">Confirm Password :</td>
                    <td>
                        <asp:TextBox ID="txtPConfirmPassword" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td>
                        
                        <asp:Button ID="btnParentSubmit" runat="server" OnClick="Button1_Click" Text="Submit" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
