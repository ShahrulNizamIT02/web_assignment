﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace WebAssignment
{
    public partial class UploadPhoto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                lblName.Text = Request.QueryString["name"];
                imgPhoto.ImageUrl = "~/Students" + lblName.Text + ".jpg"; 
            }
        }

        protected void btn_Upload_Click(object sender, EventArgs e)
        {
            string uploadedFile = "";

            if(upPhoto.HasFile==true)
            {
                string savePath;

                string fileExt = Path.GetExtension(upPhoto.FileName);

                uploadedFile = lblName.Text + fileExt;

                savePath = MapPath("~/Students/" + uploadedFile);

                try
                {
                    upPhoto.SaveAs(savePath);
                    lblMsg.Text = "File uploaded successfully";
                    imgPhoto.ImageUrl = "~/Students/" + uploadedFile;

                }
                catch(IOException)
                {
                    lblMsg.Text="File failed to upload. Please try again";

                }
                catch(Exception ex)
                {
                    lblMsg.Text = "File failed to upload. Please try again";
                }



            }

            else
            {
                lblMsg.Text = "No file is found";

            }
        }
    }
}