﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MentorMenu.ascx.cs" Inherits="WebAssignment.MentorMenu" %>
<nav class="navbar navbar-expand-md bg-light navbar-light">
    <a class ="navbar-brand" href="MentorHome.aspx"
        style="font-size:32px; font-weight:bold;color:#3399FF; font-family: 'Britannic Bold';">
        ABC Polytechnic
    </a>
    <button class="navbar-toggler"type="button"
        data-toggle="collapse"data-target="#staffNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="staffNavbar">
        <ul class="navbar-nav mr-auto">			 
                  <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Mentors
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="MentorStudentPortfolio.aspx">View Student's Portfolio</a>
          <a class="dropdown-item" href="MentorMessage.aspx">Create Portfolio</a>
            <a class="dropdown-item" href="MentorPasswordChange.aspx">Account Settings</a>
          </div>
      </li>          
                
		</ul>  
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <asp:Button ID="btnLogOut" runat="server" Text="Log Out"
                    CssClass="btn btn-link nav-link" CausesValidation="False" OnClick="btnLogOut_Click" />
            </li>
        </ul>
    </div>
</nav>