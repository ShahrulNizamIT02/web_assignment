﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WebAssignment.App_Code
{
    public class Mentor
    {
        public int UpdateStatus(int studentno,string status)
        {
            string strConn = ConfigurationManager.ConnectionStrings["ABCConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("Update Student SET Status = @newStatus WHERE StudentID = @studentid ",conn);
            cmd.Parameters.AddWithValue("@newStatus", status);
            cmd.Parameters.AddWithValue("@selectedStudentNo", studentno);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            return 0;

        }
    }
}