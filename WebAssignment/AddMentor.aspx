﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdmin.Master" AutoEventWireup="true" CodeBehind="AddMentor.aspx.cs" Inherits="WebAssignment.AddMentor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style2 {
            width: 100%;
        }
        .auto-style3 {
            color: #CC0000;
            width: 603px;
        }
        .auto-style4 {
            width: 444px;
        }
        .auto-style5 {
            width: 603px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="auto-style2">
        <tr>
            <td class="auto-style4">&nbsp;</td>
            <td class="auto-style3"><strong>Create Mentor Account</strong></td>
        </tr>
        <tr>
            <td class="auto-style4">Initial(Mr/Mrs/Ms/Mdm):</td>
            <td class="auto-style5">
                <asp:DropDownList ID="ddlInitial" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="auto-style4">Name: </td>
            <td class="auto-style5">
                <asp:TextBox ID="txtName" runat="server" Width="230px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="*Please fill in your name" ControlToValidate="txtName" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style4">Email</td>
            <td class="auto-style5">
                <asp:TextBox ID="txtEmail" runat="server" Width="230px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="*Please enter a valid email address" ControlToValidate="txtEmail" Display="Dynamic"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style4">&nbsp;</td>
            <td class="auto-style5">
                <asp:Button ID="btnConfirm" runat="server" Text="Confirm" Width="109px" OnClick="btnConfirm_Click" />
                <asp:Label ID="lblMsg" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
