﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAssignment
{
    public partial class SysAdmin : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            if (Session["LoginID"] != null)
            {
                lblLoginID.Text = (string)Session["LoginID"];


            }
           
        }

        protected void btnCreateMentor_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddMentor.aspx");
        }

        protected void btnCreateStudent_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddStudent.aspx");
        }

        protected void btnCreateSkillset_Click(object sender, EventArgs e)
        {
            Response.Redirect("CreateSkillset.aspx");
        }

        protected void btnUploadPhoto_Click(object sender, EventArgs e)
        {
            Response.Redirect("UploadPhoto.aspx");

        }

        protected void btnApproveRequest_Click(object sender, EventArgs e)
        {
            Response.Redirect("ApproveViewingRequest.aspx");
        }
    }
}