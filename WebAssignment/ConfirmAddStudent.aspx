﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdmin.Master" AutoEventWireup="true" CodeBehind="ConfirmAddStudent.aspx.cs" Inherits="WebAssignment.ConfirmAddStudent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p>
        &nbsp;The following student has been added successfully:</p>
    <p>
        ID: <asp:Label ID="lblID" runat="server"></asp:Label>
    </p>
    <p>
        Name:<asp:Label ID="lblName" runat="server"></asp:Label>
    </p>
    <p>
        Email:<asp:Label ID="lblEmail" runat="server"></asp:Label>
    </p>
    <p>
        Course:
        <asp:Label ID="lblCourse" runat="server"></asp:Label>
    </p>
    <p>
        <asp:Button ID="btnConfirm" runat="server" Text="Confirm" OnClick="btnConfirm_Click" />
    </p>
</asp:Content>
