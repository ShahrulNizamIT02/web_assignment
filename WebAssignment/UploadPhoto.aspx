﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdmin.Master" AutoEventWireup="true" CodeBehind="UploadPhoto.aspx.cs" Inherits="WebAssignment.UploadPhoto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style2 {
            width: 100%;
            margin-bottom: 0px;
        }
        .auto-style3 {
            color: #FF0000;
            height: 16px;
        }
        .auto-style7 {
            width: 387px;
            height: 51px;
        }
        .auto-style8 {
            height: 51px;
        }
        .auto-style11 {
            width: 387px;
            height: 41px;
        }
        .auto-style12 {
            height: 41px;
        }
        .auto-style15 {
            width: 387px;
            height: 16px;
        }
        .auto-style16 {
            width: 387px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="auto-style2">
        <tr>
            <td class="auto-style15"></td>
            <td class="auto-style3"><strong>Upload Student Photo</strong></td>
        </tr>
        <tr>
            <td class="auto-style16">Student Name:</td>
            <td>
                <asp:Label ID="lblName" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style16">&nbsp;</td>
            <td>
                <br />
                <br />
                <asp:Image ID="imgPhoto" runat="server" Height="100px" Width="100px" />
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td class="auto-style7">Photo: (jpg. format) </td>
            <td class="auto-style8">
                <asp:FileUpload ID="upPhoto" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="auto-style11"></td>
            <td class="auto-style12">
                <asp:Button ID="btn_Upload" runat="server" Text="Upload" Width="141px" OnClick="btn_Upload_Click" />
                <asp:Label ID="lblMsg" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
