﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MentorTemplate.Master" AutoEventWireup="true" CodeBehind="MentorPasswordChange.aspx.cs" Inherits="WebAssignment.MentorPasswordChange" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 40px;
        }
        .auto-style2 {
            width: 100%;
            height: 120px;
        }
        .auto-style3 {
            height: 40px;
            width: 294px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="auto-style2">
        <tr>
            <td class="auto-style3">
                <asp:Label ID="lblOldPass" runat="server" Text="Old Password:"></asp:Label>
            </td>
            <td class="auto-style1">
                <asp:TextBox ID="txtOldPassword" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">
                <asp:Label ID="NewPass" runat="server" Text="New Password:"></asp:Label>
            </td>
            <td class="auto-style1">
                <asp:TextBox ID="txtNewPassword" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">
                <asp:Label ID="lblConfirmPass" runat="server" Text="Confirm Password"></asp:Label>
            </td>
            <td class="auto-style1">
                <asp:TextBox ID="txtConfirmPassword" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">
                <asp:Label ID="lblPassMessage" runat="server"></asp:Label>
            </td>
            <td class="auto-style1">
                <asp:Button ID="btnChangePassword" runat="server" OnClick="btnChangePassword_Click" Text="Submit" />
            </td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td class="auto-style1">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style3">
                <asp:Label ID="lblResetPass" runat="server" Text="Reset To System Default Password"></asp:Label>
                :</td>
            <td class="auto-style1">
                <asp:Button ID="btnReset" OnClientClick="return confirm('are you sure?');" runat="server" OnClick="btnReset_Click" Text="Reset" />
            </td>
        </tr>
    </table>
</asp:Content>
