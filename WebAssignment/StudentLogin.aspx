﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="StudentLogin.aspx.cs" Inherits="WebAssignment.StudentLogin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
            <table style="border: 1px solid #000000; margin: auto; width: auto" >
            <tr>
                <td style="vertical-align: top; width: 50%;font-size:32px; font-weight:bold; color:#ff6a00;">
                    ABC Polytechnic 
                    </td>
                <td style="width: 50%">Login ID:<br />
                    <asp:TextBox ID="txtLoginID" runat="server" BackColor="#FFFFCC" TextMode="Email" ToolTip="e-mail address"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvLoginID" runat="server" ControlToValidate="txtLoginID" ErrorMessage="Please enter an ID" ForeColor="Red"></asp:RequiredFieldValidator>
                    <br />
                    Password:<br />
                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" BackColor="#FFFFCC"></asp:TextBox>
                    
                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword" ErrorMessage="Please enter your password" ForeColor="Red"></asp:RequiredFieldValidator>
                    
                    <br />
                    
                    <asp:Button ID="btnLogin" runat="server" Text="Login" OnClick="btnLogin_Click" CssClass="btn btn-outline-primary"  />
                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
                </table>
        </div>
   
</asp:Content>

