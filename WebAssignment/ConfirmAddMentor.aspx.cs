﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAssignment
{
    public partial class ConfirmMentor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMentorID.Text = Request.QueryString["id"];
            lblinitial.Text = Request.QueryString["initial"];   
            lblName.Text = Request.QueryString["name"];
            lblEmail.Text = Request.QueryString["email"];

        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            Response.Redirect("Aboutus.aspx");
        }
    }
}