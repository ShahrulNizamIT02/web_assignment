﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SysAdmin.Master" AutoEventWireup="true" CodeBehind="AddStudent.aspx.cs" Inherits="WebAssignment.AddStudent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .auto-style2 {
            font-family: "Segoe UI";
            font-weight: bold;
            padding: 0;
        }
        .auto-style3 {
            width: 200px;
            height: 26px;
        }
        .auto-style5 {
            width: 769px;
            height: 29px;
        }
        .auto-style6 {
            height: 29px;
        }
        .auto-style7 {
            height: 26px;
            color: #CC0000;
        }
        .auto-style8 {
            color: #CC0000;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form>
    <div class="row">
                <div class="col-sm-12"> <!--Definition bootstrap grid-->
                   
                        <p class="auto-style7">
                            <strong>Create Student Account</strong></p>
                    
                </div>
            </div>
           
            <!--4th row-->
            <div class="row">
                <div class="col-sm-12">

  

                    <table class="auto-style2">
                        <tr>
                            <td class="auto-style3">Name:</td>
                            <td class="auto-style5">
                                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                <strong>
                                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" CssClass="auto-style8" Display="Dynamic" ErrorMessage="*Please fill in your name" style="color: #CC0000"></asp:RequiredFieldValidator>
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style3">Mentor:</td>
                            <td class="auto-style5">
                                <asp:DropDownList ID="ddlMentor" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style6">Email:</td>
                            <td class="auto-style5">
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="auto-style8"></asp:TextBox>
                                <strong>
                                <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail" CssClass="auto-style7" Display="Dynamic" ErrorMessage="*Please enter a valid email"></asp:RegularExpressionValidator>
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style3">Course:</td>
                            <td class="auto-style5">
                                <asp:DropDownList ID="ddlCourse" runat="server">
                                    <asp:ListItem>Select A Course....</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>

  

                </div>
            </div>

       
        <asp:Button ID="btn_Confirm" runat="server" Text="Create Account" OnClick="btn_Confirm_Click" />
        <asp:Label ID="lblConfirm" runat="server"></asp:Label>
    </form>


</asp:Content>
