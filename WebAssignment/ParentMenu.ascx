﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ParentMenu.ascx.cs" Inherits="WebAssignment.ParentMenu" %>
<nav class="navbar navbar-expand-md bg-light navbar-light">
    <a class ="navbar-brand" href="Main.aspx"
        style="font-size:32px; font-weight:bold;color:#3399FF;">
        ABC Polytechnic
    </a>
    <button class="navbar-toggler"type="button"
        data-toggle="collapse"data-target="#staffNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="staffNavbar">
        <ul class="navbar-nav mr-auto">			 
                  <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Parent
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="ViewChild.aspx">View Child Portfolio</a>
          <a class="dropdown-item" href="PostMessage.aspx">Post Message</a>
            <a class="dropdown-item" href="ViewReplies.aspx">View Replies or Further Discussion</a>
          </div>
      </li>            
		</ul>  
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <asp:Button ID="btnLogOut" runat="server" Text="Log Out"
                    CssClass="btn btn-link nav-link" CausesValidation="False" OnClick="btnLogOut_Click" />
            </li>
        </ul>
    </div>
</nav>