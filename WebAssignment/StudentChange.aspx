﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student.Master" AutoEventWireup="true" CodeBehind="StudentChange.aspx.cs" Inherits="WebAssignment.StudentChange" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        
<div class="input-group mb-3">
        <h2>Change Password?</h2>
    <div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="newPass">Old Password</span>
      <asp:TextBox ID="tbOldPass" runat="server"></asp:TextBox>
      <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ErrorMessage="Enter your password" ForeColor="#CC0000" ControlToValidate="tbOldPass"></asp:RequiredFieldValidator>
  &nbsp;</div>
  &nbsp;</div>
    <div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="reEnter">New Password</span>
      <asp:TextBox ID="tbNewPass" runat="server"></asp:TextBox>
      <asp:RequiredFieldValidator ID="rfvNewPass" runat="server" ControlToValidate="tbNewPass" ErrorMessage="Enter your new password" ForeColor="Red"></asp:RequiredFieldValidator>
  </div>
         &nbsp;
        </div>
        <div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="rereEnter">Re-enter Password</span>
      <asp:TextBox ID="tbReEnter" runat="server"></asp:TextBox>
      <asp:RequiredFieldValidator ID="rfvRereEnter" runat="server" ControlToValidate="tbReEnter" ErrorMessage="Re-enter your new password" ForeColor="Red"></asp:RequiredFieldValidator>
  &nbsp;
      <asp:CompareValidator ID="cvPassword" runat="server" ControlToCompare="tbNewPass" ControlToValidate="tbReEnter" ErrorMessage="Password not match" ForeColor="Red"></asp:CompareValidator>
  </div>
            


  &nbsp;</div>
    <asp:Button ID="btnUpdate" runat="server" CssClass="btn btn-outline-success" Text="Update" OnClick="btnUpdate_Click" />
        <br />
        <asp:Button ID="btnReset" runat="server" CssClass="btn btn-outline-success" OnClick="btnReset_Click" Text="Reset" />
        <asp:Label ID="lblMsg" runat="server" ForeColor="#CC0000"></asp:Label>
</div>

        </div>
    
    
</asp:Content>

