﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAssignment
{
    public partial class ConfirmAddStudent : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblName.Text = Request.QueryString["name"];
            lblEmail.Text = Request.QueryString["email"];
            lblCourse.Text = Request.QueryString["course"];
            lblID.Text= Request.QueryString["id"];
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            Response.Redirect("Aboutus.aspx");
        }
    }
}