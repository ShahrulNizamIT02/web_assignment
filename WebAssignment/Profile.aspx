﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student.Master" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="WebAssignment.Profile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="w-100">
        <tr>
            <td><strong>Profile</strong></td>
            <td>
                <asp:Image ID="img_Photo" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="auto-style1"><span class="auto-style3"><strong>Student ID:</strong></span></td>
            <td class="auto-style1">
                <asp:Label ID="lblStudentID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1"><span class="auto-style3"><strong>Name:</strong></span></td>
            <td class="auto-style1">
                <asp:Label ID="lblName" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td><span class="auto-style3"><strong>Course:</strong> </span></td>
            <td>
                <asp:Label ID="lblCourse" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td><span class="auto-style3"><strong>Email:</strong>&nbsp;</span> </td>
            <td>
                <asp:Label ID="lblEmail" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td><span class="auto-style3"><strong>Description:</strong></span> </td>
            <td>
                <asp:Label ID="lblDescription" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td><span class="auto-style3"><strong>Achievements:</strong> </span></td>
            <td>
                <asp:Label ID="lblAchievements" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td><strong>External Link:</strong></td>
            <td>
                <asp:Label ID="lblExternalLink" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td><strong>Skill Set:</strong></td>
            <td>
                <asp:Label ID="lblSkillSet" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
