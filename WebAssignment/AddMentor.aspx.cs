﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAssignment
{
    public partial class AddMentor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                ddlInitial.Items.Add("Mr");
                ddlInitial.Items.Add("Mrs");
                ddlInitial.Items.Add("Ms");
                ddlInitial.Items.Add("Mdm");

            }           
            
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            Mentor objMentor = new Mentor();

            
            objMentor.name = ddlInitial.SelectedValue + txtName.Text;
            objMentor.email = txtEmail.Text;
           


            int id = objMentor.add();

            Response.Redirect("ConfirmAddMentor.aspx?ID= " + id +
                                   "&initial=" + ddlInitial.SelectedItem.Text +
                                   "&email=" + txtEmail.Text +
                                   "&name=" + txtName.Text);

        }
    }
}