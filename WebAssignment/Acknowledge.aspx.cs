﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WebAssignment
{
    public partial class Acknowledge : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string strConn = ConfigurationManager.ConnectionStrings["ABCConnectionString"].ToString();
                SqlConnection conn = new SqlConnection(strConn);
                SqlCommand cmd = new SqlCommand("SELECT * FROM Suggestion WHERE StudentID=@StudentID and Status = 'N'", conn);
                cmd.Parameters.AddWithValue("@StudentID", (int)Session["LoginID"]);

                //Instantiate a DataAdapter object
                SqlDataAdapter daSuggestion = new SqlDataAdapter(cmd);

                //Create dataset object
                DataSet result = new DataSet();

                //A connection must be opened before any operations made.
                conn.Open();

                //Use DataAdapter to fetch data
                //Dataset "result" will store the result of the SELECT operation.
                daSuggestion.Fill(result, "SuggestionDetails");

                //A connection should always be closed, whether error occurs or not.
                conn.Close();

                //Display all obtained student into to controls
                if (result.Tables["SuggestionDetails"].Rows.Count > 0)
                {
                    lblDateCreated.Text = ((DateTime)result.Tables["SuggestionDetails"].Rows[0]["DateCreated"]).ToString();
                    lblRemark.Text = ((String)result.Tables["SuggestionDetails"].Rows[0]["Description"].ToString());
                }
                else
                {
                    lblSuggestion.Text = "There is no suggestion";
                }
            }
        }


        protected void btnAcknowledge_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                string strConn = ConfigurationManager.ConnectionStrings["ABCConnectionString"].ToString();
                SqlConnection conn = new SqlConnection(strConn);
                SqlCommand cmd = new SqlCommand("UPDATE Suggestion Set Status = 'Y' WHERE StudentID=@selectedStudentID", conn);
                cmd.Parameters.AddWithValue("@selectedStudentID", (int)Session["LoginID"]);
                SqlCommand cmd1 = new SqlCommand("SELECT * FROM Suggestion WHERE StudentID=@StudentID and Status = 'N'", conn);
                cmd1.Parameters.AddWithValue("@StudentID", (int)Session["LoginID"]);

                //Instantiate a DataAdapter object
                SqlDataAdapter daAck = new SqlDataAdapter(cmd1);


                //Create dataset object
                DataSet result = new DataSet();

                conn.Open();

                daAck.Fill(result, "AckDetails");

                cmd.ExecuteNonQuery();
                cmd1.ExecuteNonQuery();
                conn.Close();

                if (result.Tables["AckDetails"].Rows.Count > 0)
                {
                    lblDateCreated.Text = ((DateTime)result.Tables["AckDetails"].Rows[0]["DateCreated"]).ToString();
                    lblRemark.Text = ((String)result.Tables["AckDetails"].Rows[0]["Description"].ToString());
                    lblAcknowledge.Text = "You have successfully acknowledged";
                    lblSuggestion.Text = "";
                    btnAcknowledge.Enabled = false;
                }
                else
                {
                    lblSuggestion.Text = "There is no suggestion";
                }
            }

            //if (result.Tables["AckDetails"].Rows[].
            //{
            //    lblDateCreated.Text = ((DateTime)result.Tables["AckDetails"].Rows[0]["DateCreated"]).ToString();
            //    lblRemark.Text = ((String)result.Tables["AckDetails"].Rows[0]["Description"].ToString());
            //    lblAcknowledge.Text = "You have successfully acknowledged";
            //    lblSuggestion.Text = "";
            //    btnAcknowledge.Enabled = false;               
            //}
            //else
            //{
            //    lblAcknowledge.Text = "No message seen";
            //}




        }
    }
}