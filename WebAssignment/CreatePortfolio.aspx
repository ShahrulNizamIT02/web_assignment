﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student.Master" AutoEventWireup="true" CodeBehind="CreatePortfolio.aspx.cs" Inherits="WebAssignment.CreatePortfolio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 26px;
        }
    </style>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="w-200">
    <tr>
        <td class="auto-style1"></td>
        <td class="auto-style1"><strong>Create Portfolio</strong></td>
    </tr>
    <tr>
        <td class="auto-style1">Role:</td>
        <td class="auto-style1">
            <asp:Label ID="lblRole" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style4">Member</td>
        <td>
            <asp:CheckBoxList ID="cbMember" runat="server">
            </asp:CheckBoxList>
        </td>
    </tr>
    <tr>
        <td class="auto-style4">Title</td>
        <td>
            <asp:TextBox ID="tbTitle" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvTitle" runat="server" ControlToValidate="tbTitle" ErrorMessage="Enter a title for your project" ForeColor="Red"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style1">Description</td>
        <td class="auto-style1">
            <asp:TextBox ID="tbDescription" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ControlToValidate="tbDescription" ErrorMessage="Enter a description" ForeColor="Red"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style1">Poster</td>
        <td class="auto-style1">
            <asp:FileUpload ID="upPhoto" runat="server" />
            <asp:RequiredFieldValidator ID="rfvPoster" runat="server" ControlToValidate="upPhoto" ForeColor="Red">Choose a image to upload</asp:RequiredFieldValidator>
            <asp:Label ID="lblPoster" runat="server" ForeColor="Red"></asp:Label>
            <br />
        </td>
    </tr>
    <tr>
        <td class="auto-style1">url</td>
        <td class="auto-style1">
            <asp:TextBox ID="tbUrl" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvUrl" runat="server" ControlToValidate="tbUrl" ErrorMessage="Enter a Url" ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="revUrl" runat="server" ControlToValidate="tbUrl" ErrorMessage="Enter a valid Url" ForeColor="Red" ValidationExpression="[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&amp;//=]*)"></asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style1">&nbsp;</td>
        <td class="auto-style1">
            <asp:Button ID="btnCreate" runat="server" OnClick="btnCreate_Click" Text="Create" />
            <asp:Label ID="lblMsg" runat="server" ForeColor="#CC0000"></asp:Label>
        </td>
    </tr>
</table>
<br />
</asp:Content>
