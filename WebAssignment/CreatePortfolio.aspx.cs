﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace WebAssignment
{
    public partial class CreatePortfolio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                displayMember();
                displayRole();
                string uploadedFile = "";
                if (upPhoto.HasFile == true)
                {
                    try
                    {
                        //string savePath;

                        //Find the filename extension of the file to be uploaded.
                        string fileExt = Path.GetExtension(upPhoto.FileName);

                        upPhoto.SaveAs(Server.MapPath("~/Images/") + uploadedFile);
                        lblPoster.Text = "File uploaded!";
                    }
                    catch (Exception ex) //Other type of error
                    {
                        //lblMsg.Text = ex.Message;
                        lblPoster.Text = "File could not be uploaded. The following error occured" + ex.Message;
                    }
                }
            }
            //newProjectID();

            //selectMember(projectId);

            // displayMember();


            //imgPhoto.ImageUrl = "~/Images/" + lblRole.Text + ".jpg";


            /*
            public void uploadPoster(int projectID)
            {
                if (Page.IsValid)
                {
                    string photo = upPhoto.FileName;

                    string projectName = "here";
                    string fileExt = Path.GetExtension(upPhoto.FileName);
                    string uploadedFile = projectName + fileExt;

                    string savePath = MapPath("~/Images" + uploadedFile);

                    upPhoto.SaveAs(savePath);

                    string strConn = ConfigurationManager.ConnectionStrings["ABCConnectionString"].ToString();

                    // Instantiate a SqlConnection object with the Connection String read.
                    SqlConnection conn = new SqlConnection(strConn);

                    //Instantiate a SqlCommand object, supply it with the SQL statement
                    //SELECT that operates against the database, and the connection object

                    //used for connecting to the database.(Leader          
                    SqlCommand cmd = new SqlCommand("INSERT INTO Project (ProjectPOster) VALUES (@photo)", conn);
                    cmd.Parameters.AddWithValue("@photo", uploadedFile);

                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
*/

        }


        public void displayRole()
        {
            string strConn = ConfigurationManager.ConnectionStrings["ABCConnectionString"].ToString();

            // Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

      

            //used for connecting to the database.(Leader          
            SqlCommand cmd = new SqlCommand("select Count(ProjectMember.Role) AS Role from ProjectMember " +
                "INNER JOIN Student ON ProjectMember.StudentID = Student.StudentID " +
                "Where EmailAddr = '" + Session["loginEmail"].ToString() + "' and ProjectMember.Role = 'Leader'", conn);


            //Instantiate a database object and pass the sqlCommand object
            SqlDataAdapter daStudent = new SqlDataAdapter(cmd);

            //Create a Dataset object to contain the records retrieved from database
            //DataSet result = new DataSet();
            DataTable dt = new DataTable();
            daStudent.Fill(dt);
            //Connection open
            //conn.Open();
            //Use dataAdapter to fetch data contains record from database
            //daStudent.Fill(result, "PortfolioDetails");

            //connection close

            //int found = (int)cmd.ExecuteScalar();
            //conn.Close();


            if (Convert.ToInt32(dt.Rows[0]["Role"]) == 1)
            {
                lblRole.Text = "Leader";
                //displayMember();

            }
            else
            {
                lblRole.Text = "Member";
                //ddlMember.Enabled = false;
                tbTitle.Visible = false;
                tbDescription.Visible = false;
                upPhoto.Visible = false;
                tbUrl.Visible = false;

                btnCreate.Enabled = false;
                lblMsg.Text = "Sorry, only Leader can create project";
            }
        }
  
      
        protected void btnCreate_Click(object sender, EventArgs e)
        {
       
                int ProjectID = newProjectID();

            
                //UpdateProject();
                string strConn = ConfigurationManager.ConnectionStrings["ABCConnectionString"].ToString();

                // Instantiate a SqlConnection object with the Connection String read.
                SqlConnection conn = new SqlConnection(strConn);

                //Instantiate a SqlCommand object, supply it with the SQL statement
                //SELECT that operates against the database, and the connection object

                //used for connecting to the database.(Leader          
                //SqlCommand cmd = new SqlCommand("UPDATE Project SET Title=@Title, Description=@Description, " +
                //"ProjectPoster=@Poster, ProjectURL=@Project", conn);
                SqlCommand cmd = new SqlCommand("INSERT INTO Project(Title, Description, ProjectPoster, ProjectURL) " +
                    "output inserted.Title VALUES(@Title, @Description, @Poster, @Project)", conn);


                cmd.Parameters.AddWithValue("@Title",tbTitle.Text);
                cmd.Parameters.AddWithValue("@Description", tbDescription.Text);
                cmd.Parameters.AddWithValue("@Poster", upPhoto.FileName);
                cmd.Parameters.AddWithValue("@Project", tbUrl.Text);
                conn.Open();
                //int count = 
                int count = cmd.ExecuteNonQuery();

                conn.Close();
            selectStudent(ProjectID);
           
            if (count > 0)
            {
                lblMsg.Text = "Project created successfully";
            }
            else
            {
                lblMsg.Text = "Project not created";
            }



        }

        public void displayMember()
        {
            string strConn = ConfigurationManager.ConnectionStrings["ABCConnectionString"].ToString();

            // Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object, supply it with the SQL statement
            //SELECT that operates against the database, and the connection object

            //used for connecting to the database.(Leader          
            SqlCommand cmd = new SqlCommand("SELECT * from Student", conn);
            //Create a DataAdapter object
            SqlDataAdapter daMember = new SqlDataAdapter(cmd);
            //SqlDataAdapter daAll = new SqlDataAdapter(cmd1);
            //Create DataSet object
            DataSet result = new DataSet();

            conn.Open();


            //use DataAdapter to fetch Data to a table "SkillDetails" in DataSet.
            daMember.Fill(result, "MemberDetails");

            //close database connection
            conn.Close();


           // cbMember.SelectedValue = ((string)result.Tables["MemberDetails"].Rows[0]["Name"]).ToString();
            //Specify the dropdown list to get data from dataset
            cbMember.DataSource = result.Tables["MemberDetails"];
            //Specify the Text property of dropdownlist
            cbMember.DataTextField = "Name";
            cbMember.DataValueField = "StudentID";
            //Specify the Value property of dropdownlist
            //ddlSkill.DataValueField = "SkillSetID";
            //Load Branch information to the drop-down list
            cbMember.DataBind();
        }
        
        public int newProjectID()
        {
            string strConn = ConfigurationManager.ConnectionStrings["ABCConnectionString"].ToString();

            // Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object, supply it with the SQL statement
            //SELECT that operates against the database, and the connection object

            //used for connecting to the database.(Leader          
            //SqlCommand cmd = new SqlCommand("UPDATE Project SET Title=@Title, Description=@Description, " +
            //"ProjectPoster=@Poster, ProjectURL=@Project", conn);
            SqlCommand cmd = new SqlCommand("Select MAX(ProjectID) AS ProjectID from Project", conn);
            SqlDataAdapter daProject = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            conn.Open();
            daProject.Fill(result, "Project");
            conn.Close();
            DataTable table = result.Tables["Project"];

            int ProjectID = Convert.ToInt32(table.Rows[0]["ProjectID"]);
            ProjectID += 1;


            return ProjectID;
        }
     
        public void selectStudent(int ProjectID)
        {
            List<String> memberList = new List<String>();
            foreach (ListItem member in cbMember.Items)
            {
                if (member.Selected)
                {
                    memberList.Add(member.Value);
                }
            }

            foreach (string memberID in memberList)
            {
                string strConn = ConfigurationManager.ConnectionStrings["ABCConnectionString"].ToString();

                // Instantiate a SqlConnection object with the Connection String read.
                SqlConnection conn = new SqlConnection(strConn);

                //Instantiate a SqlCommand object, supply it with the SQL statement
                //SELECT that operates against the database, and the connection object

                //used for connecting to the database.(Leader          
                SqlCommand cmd = new SqlCommand("INSERT INTO ProjectMember (ProjectID, StudentID, Role) VALUES (@ProjectID,@StudentID,'Member')", conn);
                cmd.Parameters.AddWithValue("@ProjectID", ProjectID);
                cmd.Parameters.AddWithValue("@StudentID", Convert.ToInt32(memberID));

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

            }
        }
        
    }
}

