﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student.Master" AutoEventWireup="true" CodeBehind="Acknowledge.aspx.cs" Inherits="WebAssignment.Acknowledge" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="w-100">
    <tr>
        <td class="auto-style2">&nbsp;</td>
        <td class="auto-style4"><strong>Mentor&#39;s Suggestion</strong></td>
    </tr>
    <tr>
        <td class="auto-style3">Date Created:</td>
        <td class="auto-style1">
            <asp:Label ID="lblDateCreated" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style3">Remark:</td>
        <td class="auto-style1">
            <asp:Label ID="lblRemark" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style2">&nbsp;</td>
        <td>
            <asp:Button ID="btnAcknowledge" runat="server" OnClick="btnAcknowledge_Click" Text="Acknowledge" />
        </td>
    </tr>
    <tr>
        <td class="auto-style2">&nbsp;</td>
        <td>
            <asp:Label ID="lblAcknowledge" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style3"></td>
        <td class="auto-style1">
            <asp:Label ID="lblSuggestion" runat="server"></asp:Label>
        </td>
    </tr>
</table>
</asp:Content>
