﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Configuration;

namespace WebAssignment
{
    public partial class MentorPasswordChange : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["LoginID"] == null)
            {
                Response.Redirect("MentorLogin.aspx");
            }
        }

        protected void btnChangePassword_Click(object sender, EventArgs e)
        {
            string conn = ConfigurationManager.ConnectionStrings["ABCConnectionString"].ToString();

            SqlConnection con = new SqlConnection(conn);
            //SqlDataAdapter sda = new SqlDataAdapter("SELECT Password FROM Mentor WHERE Password = '" + txtOldPassword.Text + "' ", con);
            SqlDataAdapter sda = new SqlDataAdapter("SELECT Password FROM Mentor WHERE EmailAddr = '" + Session["LoginID"].ToString() + "' ", con);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            //if (dt.Rows.Count.ToString() == "1")
            if (dt.Rows.Count == 1)
            {
                if(txtNewPassword.Text == txtConfirmPassword.Text)
                {
                    con.Open(); 
                    //SqlCommand cmd = new SqlCommand("UPDATE Mentor SET Password = '" + txtConfirmPassword.Text + "' WHERE Password = '" + txtOldPassword.Text + "' ", con);
                    SqlCommand cmd = new SqlCommand("UPDATE Mentor SET Password = '" + txtNewPassword.Text + "' WHERE EmailAddr = '" + Session["LoginID"].ToString() + "' ", con);
                    cmd.ExecuteNonQuery();
                    con.Close();
                    lblPassMessage.Text = "Successfully Updated";
                    lblPassMessage.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    lblPassMessage.Text = "New Password and Confirm Password should be the same";
                    lblPassMessage.ForeColor = System.Drawing.Color.Red;
                }
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            
            string conn = ConfigurationManager.ConnectionStrings["ABCConnectionString"].ToString();
            SqlConnection con = new SqlConnection(conn);
            //SqlDataAdapter sda = new SqlDataAdapter("SELECT Password FROM Mentor ", con);
            //DataTable dt = new DataTable();
            //sda.Fill(dt);

            con.Open();
            SqlCommand cmd = new SqlCommand("UPDATE Mentor SET Password = 'p@55Mentor' WHERE EmailAddr = '" + Session["LoginID"].ToString() + "' ", con);
            cmd.ExecuteNonQuery();
            con.Close();
            

        }
    }
}