﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace WebAssignment
{
    public class Mentor
    {
        
        public string name { get; set; }
        public string email { get; set; }
        public int mentorID { get; set; }

        public int add()
        {
            //Read file from web.config
            string strConn = ConfigurationManager.ConnectionStrings
                ["Student_EPortfolio"].ToString();

            //Initialise connection
            SqlConnection conn = new SqlConnection(strConn);


            //Makes new command and adds data into database
            SqlCommand cmd = new SqlCommand("INSERT INTO Mentor(Name, EmailAddr)" +
                "OUTPUT INSERTED.MentorID VALUES (@name, @email)", conn);


            //Define parameters with each value

            cmd.Parameters.AddWithValue("@name", name);
            cmd.Parameters.AddWithValue("@email", email);
            

            //Open connection of database
            conn.Open();

            //Retrieve auto-executed StaffID after execution of the INSERT SQL statement
            int id = (int)cmd.ExecuteScalar();

            //Close conenction of database
            conn.Close();

            return id;
        }

        public bool isEmailExist(string email)
        {

            string strConn = ConfigurationManager.ConnectionStrings
                ["Student_EPortfolio"].ToString();

            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand
                ("SELECT * FROM Mentor WHERE EmailAddr=@selectedEmail", conn);


            cmd.Parameters.AddWithValue("@selectedEmail", email);

            SqlDataAdapter daEmail = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            //Use DataAdapter to fetch data to a table "EmailDetails" in DataSet
            daEmail.Fill(result, "EmailDetails");

            conn.Close();


            if (result.Tables["EmailDetails"].Rows.Count > 0)
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        public int getDetails()
        {

            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();

            SqlConnection conn = new SqlConnection(strConn);

            SqlCommand cmd = new SqlCommand
                ("SELECT * FROM Mentor WHERE MentorID=@mentorID", conn);


            cmd.Parameters.AddWithValue("@selectedMentorID", mentorID);

            SqlDataAdapter daMentor = new SqlDataAdapter(cmd);

            DataSet result = new DataSet();

            conn.Open();

            daMentor.Fill(result, "MentorDetails");

            if (result.Tables["MentorDetails"].Rows.Count > 0)
            {
                //Fill Staff object with values from DataSet

                DataTable table = result.Tables["MentorDetails"];
                if (!DBNull.Value.Equals(table.Rows[0]["Name"]))
                {
                    name = table.Rows[0]["Name"].ToString();
                }


                return 0;

            }

            else
            {

                return -2;
            }
        }

    }
}