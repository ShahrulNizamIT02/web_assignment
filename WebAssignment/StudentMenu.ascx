﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StudentMenu.ascx.cs" Inherits="WebAssignment.StudentMenu" %>
<nav class="navbar navbar-expand-md bg-light navbar-light">
    <a class ="navbar-brand" href="Profile.aspx"
        style="font-size:32px; font-weight:bold;color:#3399FF;">
        ABC Polytechnic
    </a>
    <button class="navbar-toggler"type="button"
        data-toggle="collapse"data-target="#staffNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="staffNavbar">
        <ul class="navbar-nav mr-auto">			 
                  <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Student
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="StudentProfile.aspx">Update Profile</a>
          <a class="dropdown-item" href="CreatePortfolio.aspx">Create Project</a>
            <a class="dropdown-item" href="Acknowledge.aspx">Acknowledge Suggestions</a>
            <a class="dropdown-item" href="UpdatePortfolio.aspx">Update Portfolio</a>
            <a class="dropdown-item" href="StudentChange.aspx">Change Password</a>
          </div>
      </li>          
             <li class="nav-item">
			  <a class="nav-link" href="ViewStudent.aspx">View Group</a>
			</li>          
		</ul>  
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <asp:Button ID="btnLogOut" runat="server" Text="Log Out"
                    CssClass="btn btn-link nav-link" CausesValidation="False" OnClick="btnLogOut_Click" />
            </li>
        </ul>
    </div>
</nav>