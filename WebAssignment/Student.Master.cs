﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAssignment
{
    public partial class Student1 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["loginEmail"] !=null)
            {
                lblLoginID.Text = (string)Session["loginEmail"];
                lblLoggedInTime.Text = "You have logged in since " + Session["LoggedInTime"].ToString();
                lblCurrentUsers.Text = (string)Session["loginEmail"];
                lblCurrentUsers.Text = "There are currently " + Application["CurrentUsers"] + "users";
               
            }
            else
            {
                Response.Redirect("StudentLogin.aspx");
            }
        }
    }
}