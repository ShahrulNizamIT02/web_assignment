﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WebAssignment
{
    public partial class Profile : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            string strConn = ConfigurationManager.ConnectionStrings["ABCConnectionString"].ToString();

        // Instantiate a SqlConnection object with the Connection String read.
        SqlConnection conn = new SqlConnection(strConn);

        //Instantiate a SqlCommand object, supply it with the SQL statement
        //SELECT that operates against the database, and the connection object
        //used for connecting to the database.
        SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE EmailAddr=@email", conn);
            cmd.Parameters.AddWithValue("@email", Session["loginEmail"]);
            cmd.Parameters.AddWithValue("@studentid", (int) Session["LoginID"]);
        SqlCommand cmd1 = new SqlCommand("select SkillSetName from SkillSet " +
            "INNER JOIN StudentSkillSet ON SkillSet.SkillSetID = StudentSkillSet.SkillSetID " +
            "INNER JOIN Student ON StudentSkillSet.StudentID = Student.StudentID WHERE EmailAddr = @email",conn);
            cmd1.Parameters.AddWithValue("@email", Session["loginEmail"]);
            



            //Instantiate a DataAdapter object and pass the SqlCommand object
            // created as parameter
            SqlDataAdapter daStudent = new SqlDataAdapter(cmd);
        SqlDataAdapter daPhoto = new SqlDataAdapter(cmd);
            


        //Create a Dataset object to contain the records retrieved from database
        DataSet result = new DataSet();
        DataSet resultPic = new DataSet();
        //DataSet results = new DataSet();


            //A connection must be opened before any operations made.
            conn.Open();           
            //Use DataAdapter to fetch data to a table "PersonalDetails" in Dataset.
            //Dataset "result" will store the result of the SELECT operation.
            daStudent.Fill(result, "StudentDetails");
            daPhoto.Fill(resultPic, "ProfilePhoto");
            //daSkills.Fill(results, "SkillDetails");

            string mySkill = "";
            SqlDataReader daSkills = cmd1.ExecuteReader();
            //read
            while (daSkills.Read())
            {
                mySkill += "<br/>" + daSkills["SkillSetName"].ToString();

            }
            //A connection should always be closed, whether error occurs or not.
            conn.Close();

            //Display all obtained student into to controls
            
        lblStudentID.Text = ((int) result.Tables["StudentDetails"].Rows[0]["StudentID"]).ToString();
        lblName.Text = (result.Tables["StudentDetails"].Rows[0]["Name"] ?? "").ToString();
        lblCourse.Text = (result.Tables["StudentDetails"].Rows[0]["Course"] ?? "").ToString();
        lblEmail.Text = (string) result.Tables["StudentDetails"].Rows[0]["EmailAddr"];
        lblDescription.Text = (result.Tables["StudentDetails"].Rows[0]["Description"] ?? "").ToString();
        lblAchievements.Text = (result.Tables["StudentDetails"].Rows[0]["Achievement"] ?? "").ToString();
        lblExternalLink.Text = (result.Tables["StudentDetails"].Rows[0]["ExternalLink"] ?? "").ToString();
        img_Photo.ImageUrl = "~/Images/" + result.Tables["StudentDetails"].Rows[0]["Photo"];
            img_Photo.DataBind();
            lblSkillSet.Text = mySkill;
           
            //    List<string> newItem = new List<string> ();
            //for (int i =0;i;i++)
            //    {
            //        newItem.Add(lblSkillSet.ToString());
            //    }
            
            //lblSkillSet.Text = (results.Tables["SkillDetails"].Rows[0]["SkillSetName"] ?? "").ToString() + "<br/>" +
            //    (results.Tables["SkillDetails"].Rows[1]["SkillSetName"] ?? "").ToString() + "<br/>" +
            //    (results.Tables["SkillDetails"].Rows[2]["SkillSetName"] ?? "").ToString();



        }
    }
}