﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Menu.ascx.cs" Inherits="WebAssignment.Menu" %>

<nav class="navbar navbar-expand-md bg-light navbar-light">
    <a class="navbar-brand" href="Main.aspx"
        style="font-size:32px; font-weight:bold; color: #4499FF;">

        ABC Polytechnic

    </a>


    <button class="navbar-toggler" type="button"
        data-toggle="collapse" data-target="#staffNavbar">

        <span class="navbar-toggler-icon"></span>

    </button>

    <div class="collapse navbar-collapse" id="staffNavbar">
    <!--Links that are alignted to the left, mr-auto: right margin is automatically adjusted-->

    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a class="nav-link" href="Login.aspx">Login</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="Aboutus.aspx">About Us</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="Browseportfolio.aspx">Browse Student Portfolio</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="Contactus.aspx">Contact Us</a>
        </li>

        

    </ul>

    <!--Links that are aligned to the right, ml-auto: left margin auto-adjusted-->

    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <!--A web form control button for logging out user-->
            <asp:Button ID="btnLogOut" runat="server" Text="Log Out" 
                CssClass="btn btn-link nav-link"/>

        </li>



    </ul>


</div>



</nav>