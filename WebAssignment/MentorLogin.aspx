﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="MentorLogin.aspx.cs" Inherits="WebAssignment.MentorLogin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div>
            <table style="border: 1px solid #000000; margin: auto; width: auto" >
            <tr>
                <td style="vertical-align: top; width: 50%;font-size:32px; font-weight:bold; color:#ff6a00;">
                    ABC Polytechnic 
                    </td>
                <td style="width: 50%">Login ID:<br />
                    <asp:TextBox ID="txtLoginID" runat="server" BackColor="#FFFFCC" TextMode="Email" ToolTip="e-mail address"></asp:TextBox>
                    <br />
                    Password:<br />
                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" BackColor="#FFFFCC"></asp:TextBox>
                    
                    <br />
                    
                    <asp:Button ID="btnLogin" runat="server" Text="Login" OnClick="btnLogin_Click" CssClass="btn btn-outline-primary"  />
                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
                </table>
        </div>
   
</asp:Content>
