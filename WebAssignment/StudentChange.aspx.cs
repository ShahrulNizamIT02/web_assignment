﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Configuration;


namespace WebAssignment
{
    public partial class StudentChange : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                string conn = ConfigurationManager.ConnectionStrings["ABCConnectionString"].ToString();

                SqlConnection con = new SqlConnection(conn);
                //SqlDataAdapter sda = new SqlDataAdapter("SELECT Password FROM Mentor WHERE Password = '" + txtOldPassword.Text + "' ", con);
                SqlDataAdapter sda = new SqlDataAdapter("SELECT Password FROM Student WHERE EmailAddr = '" + Session["loginEmail"].ToString() + "' ", con);
            
                DataTable dt = new DataTable();
                sda.Fill(dt);
                //  con.Open();

                //var id = sda.
                //con.Close();

                if (tbOldPass.Text == dt.Rows[0]["Password"].ToString() )
                {


                    //if (dt.Rows.Count.ToString() == "1")
                    if (dt.Rows.Count == 1)
                    {
                        if (tbNewPass.Text == tbReEnter.Text)
                        {
                            con.Open();
                            //SqlCommand cmd = new SqlCommand("UPDATE Mentor SET Password = '" + txtConfirmPassword.Text + "' WHERE Password = '" + txtOldPassword.Text + "' ", con);
                            SqlCommand cmd = new SqlCommand("UPDATE Student SET Password = '" + tbNewPass.Text + "' WHERE EmailAddr = '" + Session["loginEmail"].ToString() + "' ", con);
                            cmd.ExecuteNonQuery();
                            con.Close();
                            lblMsg.Text = "Successfully Updated";
                            lblMsg.ForeColor = System.Drawing.Color.Green;
                        }
                        else
                        {
                            lblMsg.Text = "New Password and Confirm Password should be the same";
                            lblMsg.ForeColor = System.Drawing.Color.Red;
                        }
                    }
                
                      
                }
                else
                {
                    lblMsg.Text = "Old Password is incorrect";

                }
                    
            }
                    
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            string conn = ConfigurationManager.ConnectionStrings["ABCConnectionString"].ToString();
            SqlConnection con = new SqlConnection(conn);
            //SqlDataAdapter sda = new SqlDataAdapter("SELECT Password FROM Mentor ", con);
            //DataTable dt = new DataTable();
            //sda.Fill(dt);

            con.Open();
            SqlCommand cmd = new SqlCommand("UPDATE Student SET Password = 'p@55Student' WHERE EmailAddr = '" + Session["loginEmail"].ToString() + "' ", con);
            cmd.ExecuteNonQuery();
            con.Close();
            lblMsg.Text = "Password successfully reset";
            lblMsg.ForeColor = System.Drawing.Color.Purple;

        }

    
    }
}