﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WebAssignment
{
    public partial class StudentProfile : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                displayStudentList();
                displayCheckBoxList();
            }
        }
        private void displayStudentList()
        {
            //read connection string
            string strConn = ConfigurationManager.ConnectionStrings["ABCConnectionString"].ToString();

            //Instantiate a SqlConnection with the connection string read
            SqlConnection conn = new SqlConnection(strConn);

            //Select the commands that operate the database
            SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE EmailAddr=@email", conn);
           
            cmd.Parameters.AddWithValue("@email", Session["loginEmail"]);

            //Instantiate a database object and pass the sqlCommand object
            SqlDataAdapter daStudent = new SqlDataAdapter(cmd);

            //Create a Dataset object to contain the records retrieved from database
            DataSet result = new DataSet();

            //Connection open
            conn.Open();
            //Use dataAdapter to fetch data contains record from database
            daStudent.Fill(result, "StudentDetails");
          
            //connection close
            conn.Close();

            //Display record retrieve from student table into the textbox
            lblStudentID.Text = ((int)result.Tables["StudentDetails"].Rows[0]["StudentID"]).ToString();
            lblName.Text = (result.Tables["StudentDetails"].Rows[0]["Name"]).ToString();
            lblCourse.Text = (result.Tables["StudentDetails"].Rows[0]["Course"]).ToString();
            lblEmail.Text = (string)result.Tables["StudentDetails"].Rows[0]["EmailAddr"];
            tbDescription.Text = (result.Tables["StudentDetails"].Rows[0]["Description"] ?? "").ToString();
            tbAchievement.Text = (result.Tables["StudentDetails"].Rows[0]["Achievement"] ?? "").ToString();
            tbExternalLink.Text = (result.Tables["StudentDetails"].Rows[0]["ExternalLink"] ?? "").ToString();
           
      
        }
        
        private void displayCheckBoxList()
        {
            string strConn = ConfigurationManager.ConnectionStrings["ABCConnectionString"].ToString();

            //instantiate a sqlconnection with the connection string read
            SqlConnection conn = new SqlConnection(strConn);

            //Select the commands that operate the database
            SqlCommand cmd = new SqlCommand("SELECT SkillSetName From SkillSet", conn);
            cmd.Parameters.AddWithValue("@email", Session["loginEmail"]);
            //SqlCommand cmd1 = new SqlCommand("Select * from SkillSetName From SkillSet Where EmailAddr =@email");
            //cmd1.Parameters.AddWithValue("@email", Session["loginEmail"]);
            //Create a DataAdapter object
            SqlDataAdapter daSkill = new SqlDataAdapter(cmd);
            //SqlDataAdapter daAll = new SqlDataAdapter(cmd1);
            //Create DataSet object
            DataSet result = new DataSet();
            
            conn.Open();


            //use DataAdapter to fetch Data to a table "SkillDetails" in DataSet.
            daSkill.Fill(result, "SkillDetails");

            //close database connection
            conn.Close();
            

            cbSkill.SelectedValue = ((string)result.Tables["SkillDetails"].Rows[0]["SkillSetName"]).ToString();
            //Specify the dropdown list to get data from dataset
            cbSkill.DataSource = result.Tables["SkillDetails"];
            //Specify the Text property of dropdownlist
            cbSkill.DataTextField = "SkillSetName";
            
            //Specify the Value property of dropdownlist
            //ddlSkill.DataValueField = "SkillSetID";
            //Load Branch information to the drop-down list
            cbSkill.DataBind();


        }
        //private void selectedSkillSet()
        //{
        //    List<String> skillsetList = new List<String>();
        //    foreach (ListItem skillSet in cbSkill.Items)
        //    {
        //        if(skillSet.Selected)
        //        {
        //            skillsetList.Add(skillSet.Value);
        //        }
        //    }
        //}



        /*
        protected void cuzEmail_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (Page.IsValid) //other client-side validation passes
            {
                //read connection string
                string strConn = ConfigurationManager.ConnectionStrings["ABCConnectionString"].ToString();

                //Instantiate a SqlConnection with the connection string read
                SqlConnection conn = new SqlConnection(strConn);

                //Select the commands that operate the database
                SqlCommand cmd = new SqlCommand("SELECT COUNT(EmailAddr) As NumEmail FROM Student WHERE EmailAddr =@selectedEmail", conn);
                cmd.Parameters.AddWithValue("@selectedEmail", tbEmail.Text);

                SqlDataAdapter daEmail = new SqlDataAdapter(cmd);
                DataSet result = new DataSet();

                conn.Open();
                //Use DataAdapter to fetch to a table "EmailDetails" in DataSet
                daEmail.Fill(result, "EmailDetails");
                conn.Close();


                //if (result.Tables["EmailDetails"].Rows.Count > 0)
                if (Convert.ToInt32(result.Tables["EmailDetails"].Rows[0]["NumEmail"]) > 0)
                {
                    args.IsValid = false; //raise error
                    lblMessage.Text = "Email given exist";
                }
                else
                {
                    args.IsValid = true; //no error
                    lblMessage.Text = "Email given does not exist";
                }

            }
        }
       
       
        private void updateCheckbox()
        {
            string strConn = ConfigurationManager.ConnectionStrings["ABCConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd1 = new SqlCommand
                    ("UPDATE SkillSet SET SkillSetName = @SkillSetName FROM SkillSet" +
                    "INNER JOIN StudentSkillSet ON SkillSet.SkillSetID = StudentSkillSet.SkillSetID " +
                    "INNER JOIN Student ON StudentSkillSet.StudentID = Student.StudentID WHERE Student.StudentID = @StudentID", conn);
            cmd1.Parameters.AddWithValue("@SkillSetName", cbSkill.SelectedValue);
            cmd1.Parameters.AddWithValue("@StudentID", (int)Session["LoginID"]);
            conn.Open();
            cmd1.ExecuteNonQuery();
            conn.Close();
            

        }  */
        private void updateSkillSet()
        {
            string strConn = ConfigurationManager.ConnectionStrings["ABCConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd1 = new SqlCommand
                    ("Update StudentSkillSet SET SkillSetID = @SkillSetID FROM SkillSet, Student" +
                    "WHERE Student.EmailAddr = @StudentID; ", conn);
            cmd1.Parameters.AddWithValue("@SkillSetID", Convert.ToInt32(cbSkill.SelectedValue));
            cmd1.Parameters.AddWithValue("@StudentID", (int)Session["loginEmail"]);
            conn.Open();
            int count = cmd1.ExecuteNonQuery();
            //conn.Close();
            
            


        }
        private void updateOther()
        {
            string strConn = ConfigurationManager.ConnectionStrings["ABCConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand
                ("UPDATE Student SET Description=@Description,Achievement=@Achievement,ExternalLink=@ExternalLink WHERE StudentID=@StudentID", conn);
            
            //cmd.Parameters.AddWithValue("@EmailAddr", tbEmail.Text);
            cmd.Parameters.AddWithValue("@Description", tbDescription.Text);
            cmd.Parameters.AddWithValue("@Achievement", tbAchievement.Text);
            cmd.Parameters.AddWithValue("@ExternalLink", tbExternalLink.Text);
            cmd.Parameters.AddWithValue("@StudentID", (int)Session["LoginID"]);

            //foreach(ListItem i in cbSkill.Items)
            //{
            //    if(cbSkill.Selected == true)
            //    {

            //    }
            //}

            conn.Open();
            int count = cmd.ExecuteNonQuery();

            conn.Close();

            if (count > 0) 
            {
                lblMessage.Text = "Profile is updated successfully";
            }
            else
            {
                lblMessage.Text = "Profile is not updated";
            }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    updateSkillSet();
                    updateOther();
                }
                else
                {
                    lblMessage.Text = "Something is not quite right!";
                }
            }
            catch
            {
                return;
            }
               

        }

    
    }
}