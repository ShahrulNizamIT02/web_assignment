﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace WebAssignment
{
    public class Student
    {
        public int studentID { get; set; }
        public string name { get; set; }
        public string course { get; set; }  
        public string email { get; set; }
        public int mentorID { get; set; }

        public int add()
        {
            //Read file from web.config
            string strConn = ConfigurationManager.ConnectionStrings
                ["Student_EPortfolio"].ToString();

            //Initialise connection
            SqlConnection conn = new SqlConnection(strConn);


            //Makes new command and adds data into database
            SqlCommand cmd = new SqlCommand("INSERT INTO Student(Name, Course, EmailAddr, MentorID)" +
                "OUTPUT INSERTED.StudentID VALUES (@name, @course, @email, @mentorID)", conn);


            //Define parameters with each value
          
            cmd.Parameters.AddWithValue("@name", name);
            cmd.Parameters.AddWithValue("@course",course);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@mentorID", mentorID);

            //Open connection of database
            conn.Open();

            //Retrieve auto-executed StaffID after execution of the INSERT SQL statement
            int id = (int)cmd.ExecuteScalar();

            //Close conenction of database
            conn.Close();

            return id;
        }

        public bool isEmailExist(string email)
        {

            string strConn = ConfigurationManager.ConnectionStrings
                ["Student_EPortfolio"].ToString();

            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand
                ("SELECT * FROM Student WHERE EmailAddr=@selectedEmail", conn);


            cmd.Parameters.AddWithValue("@selectedEmail", email);

            SqlDataAdapter daEmail = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            //Use DataAdapter to fetch data to a table "EmailDetails" in DataSet
            daEmail.Fill(result, "EmailDetails");

            conn.Close();


            if (result.Tables["EmailDetails"].Rows.Count > 0)
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        public int getDetails()
        {

            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();

            SqlConnection conn = new SqlConnection(strConn);

            SqlCommand cmd = new SqlCommand
                ("SELECT * FROM Student WHERE StudentID=@selectedStudentID", conn);


            cmd.Parameters.AddWithValue("@selectedStudentID", studentID);

            SqlDataAdapter daStudent = new SqlDataAdapter(cmd);

            DataSet result = new DataSet();

            conn.Open();

            daStudent.Fill(result, "StudentDetails");

            if (result.Tables["StudentDetails"].Rows.Count > 0)
            {
                

                DataTable table = result.Tables["StudentDetails"];
                if (!DBNull.Value.Equals(table.Rows[0]["Name"]))
                {
                    name = table.Rows[0]["Name"].ToString();
                }
                

                return 0;

            }

            else
            {

                return -2;
            }
        }

    }
}